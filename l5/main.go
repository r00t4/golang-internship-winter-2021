package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
)

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/{book_id}/{id}", func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		bookID := vars["book_id"]
		id := vars["id"]
		if request.Method == "GET" {
			writer.Write([]byte("method not allowed"))
			return
		}
		body, err := ioutil.ReadAll(request.Body)
		if err != nil {
			writer.Write([]byte(err.Error()))
			return
		}

		writer.WriteHeader(201)
		writer.Write([]byte(bookID + " " + id + " " + string(body)))
	})

	//http.Handle("/example", ExampleHandler{})

	fmt.Println("server started")
	http.ListenAndServe(":8080", r)
}


type ExampleHandler struct {
}

func (e ExampleHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	if request.Method == "GET" {
		writer.Write([]byte("method not allowed"))
		return
	}
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		writer.Write([]byte(err.Error()))
		return
	}

	writer.WriteHeader(201)
	writer.Write(body)
}
