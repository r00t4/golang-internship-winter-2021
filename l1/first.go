package main

import (
		"fmt"
	)

func main() {
	// var a = 0
	// s := "asdf"
	// b := 0.1
	// ok := false
	
	// int, string, float
	// items := []string{"asd", "dsa", "123"}
	// cin := bufio.NewReader(os.Stdin)
	// fmt.Fscan(cin, &any_variable)

	a := 0
	b := 0
	fmt.Scanln(&a, &b)
	sl := make([]string, 5)

	fmt.Println(cap(sl))
	for _, val := range sl {
		fmt.Println(val)
	}
	g, ok := add(a, b)
	if !ok {
		fmt.Println("wrong input")
		return
	}
	fmt.Println(g)
}

func add(a int, b int) (int, bool) {
	if a > 1000 || b > 1000 {
		return -1, false
	} else if a < 0 || b < 0 {
		return -1, false
	} else {
		return a+b, true
	}
}