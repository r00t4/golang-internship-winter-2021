package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

func createFile() {
	//defer func()
	panic("error")
	//_, err := os.Create("%asd")
	//if err != nil {
	//	panic("err: " + err.Error())
	//}
}

func main() {
	defer func() {
		rec := recover()
		if rec != nil {
			fmt.Println("main panic:", rec)
		}
	}()
	//createFile()
	ch := make(chan bool, 2)
	ch <- true
	ch <- true
	close(ch)

	for elem := range ch {
		fmt.Println(elem)
	}
	// database connection
	// defer db.closeConnection
	// panic
	// db.closeConnection


	if strings.Contains("Helloworld", "world") {
		fmt.Println("Helloworld contains world")
	}
	words := []string{"Hello", "world", "Robot"}
	fmt.Println(strings.Join(words, ", "))
	str := ""
	str += "asd"
	strings.ToLower("Hello")
	strings.ToUpper("World")

	fmt.Printf("Equal to: %d, %f, %s, %v, %T\n", 10, 10.0, "10", "10", "10")

	getme := GetMeResult{
		ID:        0,
		IsBot:     true,
		FirstName: "Goal",
		Username:  "goal",
	}
	res, err := json.Marshal(getme)
	if err != nil {
		panic(err)
	}
	fmt.Println("Marshalled json:", string(res))
}

type GetMeResult struct {
	ID    int `json:"id"` //' ' | ` ` " "
	IsBot bool `json:"is_bot"`
	FirstName string `json:"first_name,omitempty"`
	Username string `json:"username"`
}

type GetMeResponse struct {
	OK bool `json:"ok"`
	Result GetMeResult `json:"result"`
}
