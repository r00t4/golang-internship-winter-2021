package lib

import (
	"fmt"
	"strconv"
)

type Person struct {
	Age int
	Name string
}

func NewPerson(age int, name string) *Person {
	return &Person{age, name}
}

func (p *Person) ToString() string {
	return p.Name + " " + strconv.Itoa(p.Age)
}

type Student struct {
	Person
	ID int
}

func NewStudent(id, age int, name string) *Student {
	return &Student{
		Person: *NewPerson(age, name),
		ID:     id,
	}
}

func (st *Student) ToString() string {
	return fmt.Sprintf("student: %d %s", st.ID, st.Person.ToString())
}

func (st *Student) String() string {
	return "student: " + strconv.Itoa(st.ID) + " " + st.Person.ToString()
}

type Shape interface {
	Area() int
	Perimeter() int
}

type Cube struct {
	a int
}

func NewCube(a int) *Cube {
	return &Cube{a: a}
}

func (c *Cube) Area() int {
	return 0
}

func (c *Cube) Perimeter() int {
	return 0
}

func PrintArea(sh Shape) {
	fmt.Println(sh.Area())
}