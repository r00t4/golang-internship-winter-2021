package lib

type Node struct {
	Val int
	Next *Node
}

func NewNode(val int) *Node {
	return &Node{
		Val: val,
	}
}

func (n *Node) Add(val int) {
	if n == nil {
		NewNode(val)
		return
	}
	if n.Next != nil {
		n = n.Next
		n.Add(val)
	}
	n.Next = NewNode(val)
}

// struct linked list
// delete add print