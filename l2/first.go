package main

import (
	"fmt"
	"gitlab.com/golang-internship-2021-winter/l2/lib"
)

func main() {
	cb := lib.NewCube(4)
	lib.PrintArea(cb)

	fmt.Println("done")

	st := lib.Student{
		Person: *lib.NewPerson(20, "Mike"),
		ID:     12,
	}
	fmt.Println(st.Age, st.Person.Age)
	fmt.Println(st)
	fmt.Println(st.ToString())
}

func min4(a, b, c, d int) int {
	return min(min(a, b), min(c,d))
}

func min(a,b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}


