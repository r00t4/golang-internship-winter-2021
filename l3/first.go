package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/golang-internship-2021-winter/l3/lib"
	"io/ioutil"
	"net/http"
)

func incr(a *int) {
	*a += 1
}

func main() {
	//a := 0
	//incr(&a)
	//// chan
	//first := make(chan bool)
	//second := make(chan bool)
	//
	////go incr(&a)
	//go func(f chan<- bool) {
	//	for i := 0; i< 100; i++ {
	//		fmt.Println("from first:", i)
	//	}
	//	f <- true
	//}(first)
	//fmt.Println("---------")
	//go func(s chan bool) {
	//	for i := 0; i< 100; i++ {
	//		fmt.Println("from second:", i)
	//	}
	//	s <- true
	//}(second)
	//fmt.Println(a)
	//fmt.Println("waiting for first and second goroutine")
	//
	//select {
	//case <- first:
	//	fmt.Println("response from first")
	//case <-time.After(time.Second ):
	//	fmt.Println("timeout first")
	//}
	//
	//select {
	//case <- second:
	//	fmt.Println("response from second")
	//case <-time.After(time.Second ):
	//	fmt.Println("timeout second")
	//}
	//close(first)
	//close(second)

	//<-first

	//// buffered chan
	//third := make(chan int, 5)
	//exit := make(chan bool)
	//go func(t chan int) {
	//	for i:=0; i<7; i++ {
	//		fmt.Println(<-t)
	//	}
	//	exit <- true
	//}(third)
	//for i:=0; i<6; i++ {
	//	fmt.Println(i)
	//	third<-i
	//}
	//
	//<-exit
	//fmt.Println("finish")

	//fmt.Println("sending reqeust")
	clt := http.Client{}
	//resp, err := clt.Get("https://google.kz")
	//if err != nil {
	//	fmt.Println("Error on get request:", err)
	//	return
	//}
	////fmt.Println(resp)
	//body, err := ioutil.ReadAll(resp.Body)
	//if err != nil {
	//	fmt.Println("Error on read body:", err)
	//	return
	//}
	//fmt.Println(string(body))

	//request, err := http.NewRequest("GET", "https://google.kz", nil)
	//if err != nil {
	//	fmt.Println("Error on new request:", err)
	//	return
	//}
	//resp, err := clt.Do(request)
	//if err != nil {
	//	fmt.Println("Error on get request:", err)
	//	return
	//}
	//body, err := ioutil.ReadAll(resp.Body)
	//if err != nil {
	//	fmt.Println("Error on read body:", err)
	//	return
	//}
	//fmt.Println(string(body))

	//str := []byte(`{"title": "foo", "body": "bar", "user_id": 1"}`)
	//request, err := http.NewRequest("POST", "https://jsonplaceholder.typicode.com/posts", bytes.NewBuffer(str))
	//if err != nil {
	//	fmt.Println("Error on new request:", err)
	//	return
	//}
	////url := fmt.Sprintf("https://jsonplaceholder.typicode.com/posts?%s=%s&%s=%s", "param1", "val1", "param2", "val2")
	//q := request.URL.Query()
	//q.Add("param1", "val1")
	//q.Add("param2", "val2")
	//request.URL.RawQuery = q.Encode()
	//
	//fmt.Println(request.URL)




	//resp, err := clt.Do(request)
	//if err != nil {
	//	fmt.Println("Error on get request:", err)
	//	return
	//}
	//body, err := ioutil.ReadAll(resp.Body)
	//if err != nil {
	//	fmt.Println("Error on read body:", err)
	//	return
	//}
	//fmt.Println(string(body))


	request, err := http.NewRequest("POST", "https://api.telegram.org/bot783483691:AAESqVsZOmtmcE3MUE2l0jBj2EaqL1mUGII/getme", nil)
	if err != nil {
		fmt.Println("Error on new request:", err)
		return
	}
	resp, err := clt.Do(request)
	if err != nil {
		fmt.Println("Error on get request:", err)
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error on read body:", err)
		return
	}

	b := &lib.GetMeResponse{}
	if err := json.Unmarshal(body, b); err != nil {
		fmt.Println("Error on parsing body:", err)
		return
	}
	fmt.Println(b)
}