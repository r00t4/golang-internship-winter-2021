package lib

type GetMeResult struct {
	ID int `json:"id"` //' ' | ` ` " "
	IsBot bool `json:"is_bot"`
	FirstName string `json:"first_name,omitempty"`
	Username string `json:"username"`
}

type GetMeResponse struct {
	OK bool `json:"ok"`
	Result GetMeResult `json:"result"`
}

// {"ID": "123"}