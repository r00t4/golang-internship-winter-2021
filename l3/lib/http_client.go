package lib

import "net/http"

type Client struct {
	ConnectionUrl ConnectionString
	clt http.Client
}

type ConnectionString string

